"""
Auth:           Jeremy Wang, University of Waterloo
Last Update:    2022-04-10
Desc:           Library for implementing various fluid properties
"""

import numpy as np
import torch

class Thermo:
    R = 8314.4621  # universal gas constant [J kmol^-1 K^-1]

    def __init__(self, name):
        """ Initializes and defines essential thermodynamic variables of the fluid
        :param name: string of the selected fluid, e.g. N2gas
        """

        self.A = None  # polynomial fit coefficients of thermally perfect specific heat capacity, i.e. c_p_IG_TP(T)
        self.a = None  # parameter in the cubic equation of state [Pa (m^3 kmol^-1)^2 K^0.5]
        self.b = None  # parameter in the cubic equation of state [m^3 kmol^-1]
        self.delta = None  # parameter in the cubic equation of state [m^3 kmol^-1]
        self.eos = None  # name of the state equation
        self.epsilon = None  # parameter in the cubic equation of state [m^6 kmol^-2]
        self.gamma = None  # ideal gas adiabatic heat ratio
        self.M = None  # molar mass [kg kmol^-1]
        self.name = name  # name of the fluid
        self.omega = None  # acentric factor
        self.p_c = None  # critical pressure [Pa]
        self.T_c = None  # critical temperature [K]

        if name == "N2gas":
            self.A = np.array([3.539, -0.261e-3, 0.007e-5, 0.157e-8, -0.099e-11])
            self.gamma = 1.39959627
            self.M = 28.014
            self.omega = 0.0370
            self.p_c = 3398000
            self.T_c = 126.20
        else:
            raise ValueError("Thermo.__init__(): name was not found.")

    def get_dThetadT(self, T: np.array) -> np.array:
        """ Computes dTheta/dT function
        :param T: np.array of temperatures [K]
        :return dThetadT: np.array of Theta derivatives [Pa m^6 kmol^-2 K^-1]
        """

        dThetadT = None

        if self.eos == "IG":
            dThetadT = 0
        elif self.eos == "RK":
            dThetadT = -0.5 * self.a * T ** (-1.5)
        elif self.eos == "SRK":
            dThetadT = -self.a * (
                    1 + (0.48 + 1.574 * self.omega - 0.176 * self.omega ** 2) * (1 - (T / self.T_c) ** 0.5)) \
                       * (0.48 + 1.574 * self.omega - 0.176 * self.omega ** 2) / (T * self.T_c) ** 0.5
        elif self.eos == "PR":
            dThetadT = -self.a * (
                    1 + (0.37464 + 1.54226 * self.omega - 0.2699 * self.omega ** 2) * (1 - (T / self.T_c) ** 0.5)) \
                       * (0.37464 + 1.54226 * self.omega - 0.2699 * self.omega ** 2) / (T * self.T_c) ** 0.5
        else:
            raise ValueError("Thermo.get_dThetadT: eos has not been set.")

        return dThetadT

    def get_e_from_v_T(self, v: np.array, T: np.array) -> np.array:
        """ Computes molar-specific internal energy
        :param v: np.array of molar specific volumes [m^3 kmol^-1]
        :param T: np.array of temperatures [K]
        :return E: np.array of internal energies [J kmol^-1]
        """

        h = self.get_h_from_v_T(v, T)
        p = self.get_p_from_v_T(v, T)
        e = h - p * v
        return e

    def get_h_from_v_T(self, v: np.array, T: np.array) -> np.array:
        """ Computes molar-specific enthalpy using departure functions
        :param v: np.array of molar specific volumes [m^3 kmol^-1]
        :param T: np.array of temperatures [K]
        :return: np.array of enthalpies [J kmol^-1]
        """

        # Calculate constant-pressure enthalpy for a thermally perfect gas
        h_IG_TP = 0
        for i in range(0, self.A.shape[0]):
            h_IG_TP += Thermo.R * self.A[i] * T ** (i + 1) / (i + 1)

        # Calculate thermodynamically consistent departure function for enthalpy
        Theta = self.get_Theta(T)
        dThetadT = self.get_dThetadT(T)
        p = self.get_p_from_v_T(v, T)

        if self.eos == "IG":
            delta_h_IG_TP = 0
        else:
            log_arg = ((self.delta**2 - 4*self.epsilon)**0.5 + 2*v + self.delta)\
                                   /((self.delta**2 - 4*self.epsilon)**0.5 - 2*v - self.delta) + 0j

            if type(p) == torch.Tensor:  # for use in PyTorch
                log_result = torch.real(torch.log(log_arg))
                delta_h_IG_TP = (Theta - T*dThetadT)*log_result/((self.delta**2 - 4*self.epsilon)**0.5) \
                                + Thermo.R*T - p*v
            else:  # for use outside PyTorch
                delta_h_IG_TP = (Theta - T*dThetadT)*np.real(np.log(log_arg))/((self.delta**2 - 4*self.epsilon)**0.5) \
                                + Thermo.R*T - p*v

        h = h_IG_TP - delta_h_IG_TP
        return h

    def get_p_from_v_T(self, v: np.array, T: np.array) -> np.array:
        """ Computes pressure
        :param v: np.array of molar-specific volumes [m^3 kmol^-1]
        :param T: np.array of temperatures [K]
        :return p: np.array of pressures [Pa]
        """

        Theta = self.get_Theta(T)
        p = Thermo.R * T / (v - self.b) - Theta / (v ** 2 + self.delta * v + self.epsilon)
        return p

    def get_T_from_p_v(self, p: np.array, v: np.array) -> np.array:
        """ Computes temperature from pressure and molar-specific volume
        :param p: np.array of pressures [Pa]
        :param v: np.array of molar-specific volumes [m^3 kmol^-1]
        :return T: np.array of temperatures [K]
        """

        if self.eos == "IG":
            T = p*v/Thermo.R
            return T

        # Use Newton-Raphson method to find T numerically
        crit_conv = 1e-6
        max_iters = 100
        T_cur = 1
        p_cur = self.get_p_from_v_T(v, T_cur)
        diff = p_cur - p
        while max_iters > 0:
            try:
                do_iterate = max(abs(diff)) > crit_conv  # an array is passed in
            except:
                do_iterate = abs(diff) > crit_conv  # scalar is passed in

            if do_iterate:
                dThetadT = self.get_dThetadT(T_cur)
                dpdT = Thermo.R / (v - self.b) - dThetadT/(v**2 + self.delta*v + self.epsilon)
                T_cur = T_cur - diff/dpdT
                p_cur = self.get_p_from_v_T(v, T_cur)
                diff = p_cur - p
                max_iters -= 1
            else:
                break

        return T_cur

    def get_Theta(self, T: np.array) -> np.array:
        """ Computes the Theta temperature function in the state equation
        :param T: np.array of temperatures [K]
        :return Theta: np.array of Theta values [Pa m^6 kmol^-2]
        """

        Theta = None

        if self.eos == "IG":
            Theta = 0
        elif self.eos == "RK":
            Theta = self.a * T ** (-0.5)
        elif self.eos == "SRK":
            Theta = self.a * (1 + (0.48 + 1.574 * self.omega - 0.176 * self.omega ** 2) * (
                    1 - (T/self.T_c) ** 0.5)) ** 2
        elif self.eos == "PR":
            Theta = self.a * (1 + (0.37464 + 1.54226 * self.omega - 0.2699 * self.omega ** 2)
                              * (1 - (T/self.T_c) ** 0.5)) ** 2
        else:
            raise ValueError("Thermo.get_Theta: eos has not been set.")

        return Theta

    def set_eos(self, eos: str):
        """ Sets properties related to the state equation
        :param eos: choice of state equation
        """

        self.eos = eos

        if self.eos == "IG":
            self.a = 0
            self.b = 0
            self.delta = 0
            self.epsilon = 0
        elif self.eos == "RK":
            self.a = 0.4278 * (Thermo.R ** 2) * (self.T_c ** 2.5) / self.p_c
            self.b = 0.0867 * Thermo.R * self.T_c / self.p_c
            self.delta = self.b
            self.epsilon = 0
        elif self.eos == "SRK":
            self.a = 0.42747 * (Thermo.R ** 2) * (self.T_c ** 2) / self.p_c
            self.b = 0.08664 * Thermo.R * self.T_c / self.p_c
            self.delta = self.b
            self.epsilon = 0
        elif self.eos == "PR":
            self.a = 0.45724 * (Thermo.R ** 2) * (self.T_c ** 2) / self.p_c
            self.b = 0.07780 * Thermo.R * self.T_c / self.p_c
            self.delta = 2 * self.b
            self.epsilon = -(self.b ** 2)
        else:
            raise ValueError("Thermo.set_eos(): eos was not found.")
