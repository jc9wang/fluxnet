# Jeremy Wang (#20757422)
# University of Waterloo
# Last Update: 2022-07-07
# Built in PyCharm Community 2021.3.2 with Python 3.9 and PyTorch 1.10.2
# Note: cross-validation not implemented in this version of this script

# Usage: set parameters and run

# ======================================================================================================================
# IMPORT

import csv
import fluids
import matplotlib.pyplot as plt
import math
import numpy as np
import pandas as pd
import torch

# ======================================================================================================================
# USER-DEFINED PARAMS
print("Defining user-defined params...")

# File management params
PATH_SAVE_LEARNING_CURVE = './curve.csv'
PATH_SAVE_MODEL = './riemann_net.pt'
PATH_SAVE_MODEL_INFO = './riemann_net.txt'
PATH_READ_DATASET = './training_data.xlsx'

# Train and test parameters
num_epochs = 8000
size_set_train = pow(2, 12)  # do 2^12 for final training
size_set_test = pow(2, 9)  # do 2^9 for final testing
random_seed = 20  # seed for generating training and test splits

# Optimizer and loss parameters
rate_learning = 2.5e-5
wt_mom = 0.9
type_loss = "mse"  # {mse, msephysics}
coeff_lambda = 1e-6  # regularization parameter for loss function
fluid_name = "N2gas"  # name of the fluid
fluid_eos = "PR"  # choice of state equation
type_optimizer = "adam"  # {sgd, adam, rmsprop}
wt_decay = 1e-9

# Input and output parameters
D_in_start = 0  # Index for start of input features
D_in_end = 5  # Index for end of input features (inclusive)
D_out_start = 6  # Index for start of output features
D_out_end = 12  # Index for end of output features (inclusive)

# Visualization parameters
fig_width_in = 14
fig_height_in = 6
fig_dpi = 100
interval_plot = 10
num_cols = 4

# ======================================================================================================================
# FUNCTIONS AND CLASSES
print("Implementing functions and classes...")

torch.set_default_tensor_type(torch.DoubleTensor)

class fluxNet(torch.nn.Module):
    def __init__(self, D_in, D_out):
        super().__init__()
        self.layer1 = torch.nn.Linear(D_in, 64)
        self.layer2 = torch.nn.Linear(64, 64)
        self.layer3 = torch.nn.Linear(64, 64)
        self.layer4 = torch.nn.Linear(64, 64)
        self.layer5 = torch.nn.Linear(64, 64)
        self.layer6 = torch.nn.Linear(64, D_out)
        self.actFcn = torch.nn.LeakyReLU()

    def forward(self, x):
        x = x.view(-1, list(x.shape)[0])
        x = self.actFcn(self.layer1(x))
        x = self.actFcn(self.layer2(x))
        x = self.actFcn(self.layer3(x))
        x = self.actFcn(self.layer4(x))
        x = self.actFcn(self.layer5(x))
        x = self.layer6(x)
        x = torch.flatten(x)
        return x


class mseLossPhysics(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.lossMSE = torch.nn.MSELoss()

    def forward(self, inputs, targets, coeff_lambda):
        # Mean-squared error loss
        loss_mse = self.lossMSE(inputs, targets)

        # Physical constraint in the loss
        rho_star_L = inputs[0]
        rho_star_R = inputs[1]
        u_star_L = inputs[2]
        u_star_R = inputs[3]
        S_CD = inputs[4]
        p_star_L = inputs[5]
        p_star_R = inputs[6]

        # Compute Rankine-Hugoniot losses (mass and momentum)
        err_RH_mass = S_CD * (rho_star_R - rho_star_L) \
                      - rho_star_R * u_star_R \
                      + rho_star_L * u_star_L
        err_RH_momentum = S_CD * (rho_star_R * u_star_R - rho_star_L * u_star_L) \
                          - (rho_star_R * pow(u_star_R, 2) + p_star_R) \
                          + (rho_star_L * pow(u_star_L, 2) + p_star_L)
        loss_RH = coeff_lambda * (abs(err_RH_mass) + abs(err_RH_momentum))

        return loss_mse + loss_RH


class mseLoss(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.lossMSE = torch.nn.MSELoss()

    def forward(self, inputs, targets):
        return self.lossMSE(inputs, targets)


# ======================================================================================================================
# PREPROCESSING
print("Preprocessing...")

# Import the raw data
data_raw = pd.read_excel(PATH_READ_DATASET)

# Normalize data based on mean normalization
data_normal = pd.DataFrame()
mean = np.zeros(data_raw.shape[1])
minima = np.zeros(data_raw.shape[1])
maxima = np.zeros(data_raw.shape[1])
for i in range(0, data_raw.shape[1]):
    minima[i] = min(data_raw.iloc[:, i])
    maxima[i] = max(data_raw.iloc[:, i])
    mean[i] = pd.DataFrame.mean(data_raw.iloc[:, i])
    data_normal_cur = (data_raw.iloc[:, i] - mean[i]) / (maxima[i] - minima[i])
    data_normal = pd.concat([data_normal, data_normal_cur], axis=1)

# Create train and test splits
subset_data_normal = data_normal.sample(n=size_set_train + size_set_test, random_state=random_seed)

subset_train = subset_data_normal.iloc[0:size_set_train, :].values  # get training rows
x_train = subset_train[:, D_in_start:D_in_end + 1]  # get training features
y_train = subset_train[:, D_out_start:D_out_end + 1]  # get training outputs

subset_test = subset_data_normal.iloc[size_set_train:size_set_train + size_set_test, :].values  # get test rows
x_test = subset_test[:, D_in_start:D_in_end + 1]  # get test features
y_test = subset_test[:, D_out_start:D_out_end + 1]  # get test outputs

x_train = torch.from_numpy(x_train) # convert to PyTorch tensor
y_train = torch.from_numpy(y_train)
x_test = torch.from_numpy(x_test)
y_test = torch.from_numpy(y_test)

# Construct fluid thermo, NN, loss function, and optimizer
fluid = fluids.Thermo(fluid_name)
fluid.set_eos(fluid_eos)
flux_net_1 = fluxNet(D_in_end - D_in_start + 1, D_out_end - D_out_start + 1)

if type_loss == "mse":
    loss_fn = mseLoss()
elif type_loss == "msephysics":
    loss_fn = mseLossPhysics()

if type_optimizer == "sgd":
    optimizer = torch.optim.SGD(flux_net_1.parameters(), lr=rate_learning, momentum=wt_mom, weight_decay=wt_decay)
elif type_optimizer == "adam":
    optimizer = torch.optim.Adam(flux_net_1.parameters(), lr=rate_learning, weight_decay=wt_decay)
elif type_optimizer == "rmsprop":
    optimizer = torch.optim.RMSprop(flux_net_1.parameters(), lr=rate_learning, momentum=wt_mom, weight_decay=wt_decay)

# Create csv file and header
header_string = "Epoch"
for i in range(0, D_out_end - D_out_start + 1):
    header_string += ",Train Error " + str(i)
for i in range(0, D_out_end - D_out_start + 1):
    header_string += ",Test Error " + str(i)
header_string += ",Train Loss"
header_string += ",Test Loss"
header_string += "\n"
with open(PATH_SAVE_LEARNING_CURVE, 'w') as writer:
    writer.write(header_string)

# ======================================================================================================================
# PERFORM TRAINING EPOCHS
print("Starting training process...")

avg_loss_train = np.zeros(num_epochs)
avg_loss_test = np.zeros(num_epochs)
avg_err_train = np.zeros((num_epochs, D_out_end - D_out_start + 1))
avg_err_test = np.zeros((num_epochs, D_out_end - D_out_start + 1))

fig = plt.figure(0, figsize=(fig_width_in, fig_height_in), dpi=fig_dpi)
plt.ion()
num_plots = D_out_end - D_out_start + 2
num_rows = math.ceil(num_plots/num_cols)

for epoch in range(num_epochs):
    loss_cml_train = 0
    err_cml_train = np.zeros(D_out_end - D_out_start + 1)
    loss_cml_test = 0
    err_cml_test = np.zeros(D_out_end - D_out_start + 1)

    # Train on training data
    for i in range(0, size_set_train):
        x_cur = x_train[i, :]
        y_cur = y_train[i, :]
        optimizer.zero_grad()  # zero the parameter gradients
        outputs = flux_net_1(x_cur)  # run inputs through the network
        loss = loss_fn(outputs, y_cur, coeff_lambda)  # compute the loss
        loss.backward()  # backprop
        optimizer.step()  # update the weights

    # Test on training data
    for i in range(0, size_set_train):
        x_cur = x_train[i, :]
        y_cur = y_train[i, :]
        outputs = flux_net_1(x_cur)  # run inputs through the network
        loss = loss_fn(outputs, y_cur, coeff_lambda)  # compute the loss
        loss_cml_train += loss.item()  # save loss
        for j in range(0, D_out_end - D_out_start + 1):
            err_cml_train[j] += abs((y_cur[j] - outputs[j]) / y_cur[j])  # save errors
    avg_loss_train[epoch] = loss_cml_train / size_set_train

    # Test on testing data
    for i in range(0, size_set_test):
        x_cur = x_test[i, :]
        y_cur = y_test[i, :]
        outputs = flux_net_1(x_cur)  # run inputs through the network
        loss = loss_fn(outputs, y_cur, coeff_lambda)  # compute the loss
        loss_cml_test += loss.item()  # save loss
        for j in range(0, D_out_end - D_out_start + 1):
            err_cml_test[j] += abs((y_cur[j] - outputs[j]) / y_cur[j])  # save errors
    avg_loss_test[epoch] = loss_cml_test / size_set_test

    # Print results
    print("[E=%5d, N=%5d] | trn error [%%] =" % (epoch + 1, size_set_train), end='')
    for i in range(0, D_out_end - D_out_start + 1):
        avg_err_train[epoch, i] = 100 * err_cml_train[i] / size_set_train
        print(" %3.5f" % avg_err_train[epoch, i], end='')

    print(" | tst error [%%]:", end='')
    for i in range(0, D_out_end - D_out_start + 1):
        avg_err_test[epoch, i] = 100 * err_cml_test[i] / size_set_test
        print(" %3.5f" % avg_err_test[epoch, i], end='')

    print(" | trn loss = %3.9f" % avg_loss_train[epoch], end='')
    print(" | tst loss: %3.9f" % avg_loss_test[epoch], end='')
    print()

    # Plot results
    if epoch % interval_plot == 0 or epoch == num_epochs - 1:
        plt.clf()
        for i in range(0, num_plots - 1):
            plt.subplot(num_rows, num_cols, i + 1)
            plt.xlabel("Epoch")
            plt.ylabel("Error {index} (%)".format(index=i))
            plt.plot(avg_err_train[:, i])
            plt.plot(avg_err_test[:, i])
            plt.ylim([0, np.max([avg_err_train, avg_err_test])])

        plt.subplot(num_rows, num_cols, num_plots)
        plt.xlabel("Epoch")
        plt.ylabel("Average Loss")
        plt.plot(avg_loss_train)
        plt.plot(avg_loss_test)
        plt.legend(["Train", "Test"])

        plt.tight_layout()
        plt.draw()
        plt.show()
        plt.pause(0.001)

    # Save training/testing progress to excel spreadsheet
    with open(PATH_SAVE_LEARNING_CURVE, 'a') as writer:
        arr_out = np.concatenate(([np.array(epoch)],
                                  avg_err_train[epoch, :],
                                  avg_err_test[epoch, :],
                                  [avg_loss_train[epoch]],
                                  [avg_loss_test[epoch]]))
        np.savetxt(writer, arr_out.reshape(1, arr_out.shape[0]), delimiter=',', comments='', fmt='%s')

# SAVE RELEVANT DATA
# ======================================================================================================================

# Save the model
print("Now saving the model...")
torch.save(flux_net_1.state_dict(), PATH_SAVE_MODEL)

# Save the model information
f = open(PATH_SAVE_MODEL_INFO, "w")

f.write("Train and test params:\n")
f.write("PATH_READ_DATASET = %s\n" % PATH_READ_DATASET)
f.write("size_set_train = %d\n" % size_set_train)
f.write("size_set_test = %d\n" % size_set_test)
f.write("random_seed = %.9f\n" % random_seed)
f.write("epochs = %d\n" % num_epochs)
f.write("\n")
f.write("Optimizer params:\n")
f.write("rate_learning = %.9f\n" % rate_learning)
f.write("wt_mom = %.9f\n" % wt_mom)
f.write("type_loss = %s\n" % type_loss)
f.write("coeff_lambda = %.9f\n" % coeff_lambda)
f.write("fluid_name = %s\n" % fluid_name)
f.write("fluid_eos = %s\n" % fluid_eos)
f.write("type_optimizer = %s\n" % type_optimizer)
f.write("wt_decay = %.9f\n" % wt_decay)
f.write("\n")
f.write("Inputs and outputs:\n")
f.write("D_in_start = %d\n" % D_in_start)
f.write("D_in_end = %d\n" % D_in_end)
f.write("Inputs = %s\n" % str(list(data_raw.columns[D_in_start:D_in_end + 1])))
f.write("D_out_start = %d\n" % D_out_start)
f.write("D_out_end = %d\n" % D_out_end)
f.write("Outputs = %s\n" % str(list(data_raw.columns[D_out_start:D_out_end + 1])))
f.write("\n")
f.write("NN layer data:\n")
f.write("%s\n" % str(flux_net_1))
f.write("\n")
f.write("Visualization params:\n")
f.write("fig_width_in = %.5f\n" % fig_width_in)
f.write("fig_height_in = %.5f\n" % fig_height_in)
f.write("fig_dpi = %.5f\n" % fig_dpi)
f.close()

# Save plots
plt.savefig("results.png")

print('Finished program!\n')
